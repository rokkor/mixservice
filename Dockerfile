FROM openjdk:18-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
#RUN apk add --no-cache tzdata
ENV TZ Asia/bangkok
ENV JAVA_OPTS=""
EXPOSE 10002/tcp
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS  -jar app.jar" ]
