package me.pixka.mixservice

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import me.pixka.mixservice.model.Mixdata
import me.pixka.mixservice.model.MixdataService
import me.pixka.mixservice.model.Searchobj
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestfindMixdata {

    var sdf = SimpleDateFormat("dd/MM/yyyy")
    @LocalServerPort
    private val port = 0


    val mapper = jacksonObjectMapper()
    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    lateinit var ms: MixdataService

    fun add(mixdate:Date = Date())
    {
        var m = Mixdata()
        m.mixdate = mixdate
        ms.save(m)
    }
    @Test
    fun testSearch()
    {
        val baseUrl = "http://localhost:${port}/api/mix/search"
        val uri = URI(baseUrl)
        val headers = HttpHeaders()

        add(sdf.parse("1/10/2022"))
        add(sdf.parse("2/10/2022"))

        val searchobj = Searchobj()
        searchobj.sdate = sdf.parse("30/9/2022")
        searchobj.edate = sdf.parse("31/10/2022")

        val request: HttpEntity<Searchobj> = HttpEntity<Searchobj>(searchobj,headers)
        val result = restTemplate!!.postForEntity(
            uri, request,
            String::class.java
        )
        println(result)
        val genres: List<Mixdata> = mapper.readValue(result.body!!)
        println(genres.size)
    }
}