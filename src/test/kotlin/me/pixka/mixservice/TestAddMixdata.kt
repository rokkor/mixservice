package me.pixka.mixservice

import me.pixka.mixservice.model.Mixdata
import me.pixka.mixservice.model.MixdataService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.math.BigDecimal

@DataJpaTest
class TestAddMixdata {
    @Autowired
    lateinit var ms: MixdataService


    @Test
    fun testAdd() {
        var m = Mixdata()
        m.checksum = BigDecimal(6.4)
        m.description = ""
        m.mix1 = BigDecimal(1223)
        ms.save(m)
        Assertions.assertTrue(m.id!!>0)
    }
}