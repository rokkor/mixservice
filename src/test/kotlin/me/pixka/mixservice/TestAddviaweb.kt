package me.pixka.mixservice

import me.pixka.mixservice.model.Mixdata
import me.pixka.mixservice.model.MixdataService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import java.net.URI

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestAddviaweb {

    @LocalServerPort
    private val port = 0


    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    lateinit var ms:MixdataService
    @Test
    fun testAddEmailviaControl() {
        val baseUrl = "http://localhost:${port}/api/mix/add"
        val uri = URI(baseUrl)
        val headers = HttpHeaders()
        var m = Mixdata()
        val request: HttpEntity<Mixdata> = HttpEntity<Mixdata>(m,headers)
        println(request)
        val result = restTemplate!!.postForEntity(
            uri, request,
            String::class.java
        )
        println(result)

        var list = ms.all()

        Assertions.assertTrue(list.size>0 )

    }

}