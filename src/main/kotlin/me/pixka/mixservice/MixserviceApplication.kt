package me.pixka.mixservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@ComponentScan(value = arrayOf("me.pixka"))
@EnableScheduling
@EnableEurekaClient
class MixserviceApplication

fun main(args: Array<String>) {
	runApplication<MixserviceApplication>(*args)
}
