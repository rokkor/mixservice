package me.pixka.mixservice.model

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Mixdata(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    var name: String? = null, var description: String? = null,
    var mix1: BigDecimal? = null, var mix2: BigDecimal? = null,
    var mix3: BigDecimal? = null, var mix4: BigDecimal? = null,
    var mix5: BigDecimal? = null, var mix6: BigDecimal? = null,
    var mix7: BigDecimal? = null, var mix8: BigDecimal? = null,
    var mix9: BigDecimal? = null, var mix10: BigDecimal? = null,
    var mix11: BigDecimal? = null, var mix12: BigDecimal? = null,
    /*ค่าสำหรับตรวจสอบ*/var checksum: BigDecimal? = null,
    /*ลำ*/var lum: BigDecimal? = null,/*รอบการผสม*/var mixcount: Int? = null,
    /*โดโลไมค์*/var dolowmai: BigDecimal? = null,
//              var waterset: BigDecimal? = null /*บอกว่าใช้น้ำเท่าไหร่*/,
    /*น้ำ*/var wateruse: BigDecimal? = null, var getfg: BigDecimal? = null,
    var mixdate: Date? = null, var wateradd: BigDecimal? = null
)

@Repository
interface MixdataRepo : JpaRepository<Mixdata, Long> {
    @Query("from Mixdata m where m.mixdate >=?1 and m.mixdate <=?2")
    fun findByDate(sdate: Date, edate: Date, page: PageRequest): List<Mixdata>?
}

@Service
class MixdataService : DefaultService<Mixdata>() {
    fun searchBydate(sdate: Date, edate: Date,page:Int=0,limit:Int=1000): List<Mixdata>? {

        var page = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "id"))
        return (repo as MixdataRepo).findByDate(sdate, edate,page)

    }
}

