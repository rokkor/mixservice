package me.pixka.mixservice.model

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.JpaRepository

open class DefaultService<T> {

    @Autowired
    lateinit var repo:JpaRepository<T,Long>

    fun save(o:T): T {
        return repo.saveAndFlush(o)
    }

    fun all(): MutableList<T> {
        return repo.findAll()
    }
}