package me.pixka.mixservice.control

import me.pixka.mixservice.model.Mixdata
import me.pixka.mixservice.model.MixdataService
import me.pixka.mixservice.model.Searchobj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/mix")
class MixdataControl(val ms:MixdataService) {

    @PostMapping("add")
    @CrossOrigin
    fun add(@RequestBody mixdata:Mixdata): Mixdata {
        return ms.save(mixdata)
    }

    @PostMapping("search")
    @CrossOrigin
    fun sn(@RequestBody search:Searchobj): List<Mixdata>? {
        return ms.searchBydate(search.sdate!!,search.edate!!,search.page!!,search.limit!!)
    }
}