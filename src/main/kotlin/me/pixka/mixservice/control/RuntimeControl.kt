package me.pixka.mixservice.control

import me.pixka.mixservice.service.RunService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/mix/")
class RuntimeControl(val runService: RunService) {

    @GetMapping("run")
    fun getRun(): Runtimeobject {
        return Runtimeobject("Mix service",runService.runtime)
    }
}

class Runtimeobject(var servicename:String?=null,var runtime:Long)