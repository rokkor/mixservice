package me.pixka.mixservice.service

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class RunService {
    var runtime: Long = 0


    @Scheduled(fixedDelay = 1000)
    fun run() {
        runtime++
    }


}